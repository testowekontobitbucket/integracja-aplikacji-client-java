/**
 * BookManagerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.zut.books.client;

public class BookManagerServiceLocator extends org.apache.axis.client.Service implements pl.edu.zut.books.client.BookManagerService {

    public BookManagerServiceLocator() {
    }


    public BookManagerServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BookManagerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BookManagerPort
    private java.lang.String BookManagerPort_address = "http://localhost:666/books";

    public java.lang.String getBookManagerPortAddress() {
        return BookManagerPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BookManagerPortWSDDServiceName = "BookManagerPort";

    public java.lang.String getBookManagerPortWSDDServiceName() {
        return BookManagerPortWSDDServiceName;
    }

    public void setBookManagerPortWSDDServiceName(java.lang.String name) {
        BookManagerPortWSDDServiceName = name;
    }

    public pl.edu.zut.books.client.BookManager getBookManagerPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BookManagerPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBookManagerPort(endpoint);
    }

    public pl.edu.zut.books.client.BookManager getBookManagerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            pl.edu.zut.books.client.BookManagerPortBindingStub _stub = new pl.edu.zut.books.client.BookManagerPortBindingStub(portAddress, this);
            _stub.setPortName(getBookManagerPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBookManagerPortEndpointAddress(java.lang.String address) {
        BookManagerPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (pl.edu.zut.books.client.BookManager.class.isAssignableFrom(serviceEndpointInterface)) {
                pl.edu.zut.books.client.BookManagerPortBindingStub _stub = new pl.edu.zut.books.client.BookManagerPortBindingStub(new java.net.URL(BookManagerPort_address), this);
                _stub.setPortName(getBookManagerPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BookManagerPort".equals(inputPortName)) {
            return getBookManagerPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://books/", "BookManagerService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://books/", "BookManagerPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BookManagerPort".equals(portName)) {
            setBookManagerPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
