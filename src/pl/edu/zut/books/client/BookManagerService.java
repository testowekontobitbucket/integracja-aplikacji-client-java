/**
 * BookManagerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.zut.books.client;

public interface BookManagerService extends javax.xml.rpc.Service {
    public java.lang.String getBookManagerPortAddress();

    public pl.edu.zut.books.client.BookManager getBookManagerPort() throws javax.xml.rpc.ServiceException;

    public pl.edu.zut.books.client.BookManager getBookManagerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
