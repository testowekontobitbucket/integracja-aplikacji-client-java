/**
 * BookManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.edu.zut.books.client;

public interface BookManager extends java.rmi.Remote {
    public pl.edu.zut.books.client.Book[] searchByTitle(java.lang.String title) throws java.rmi.RemoteException;
    public pl.edu.zut.books.client.Book[] searchByAuthor(java.lang.String author) throws java.rmi.RemoteException;
    public void addBook(pl.edu.zut.books.client.Book book) throws java.rmi.RemoteException;
    public pl.edu.zut.books.client.Book searchByISBN(java.lang.String isbn) throws java.rmi.RemoteException;
}
