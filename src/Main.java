import org.apache.axis.AxisFault;
import pl.edu.zut.books.client.Book;
import pl.edu.zut.books.client.BookManager;
import pl.edu.zut.books.client.BookManagerPortBindingStub;
import pl.edu.zut.books.client.BookManagerServiceLocator;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            BookManager bookManager = new BookManagerServiceLocator().getBookManagerPort();

            Book book = new Book();
            book.setAuthor("Autorek");
            book.setTitle("Ksiazeczka");
            book.setIsbn("n9fnu3");
            book.setPages(200);
            book.setPublisher("Ozyrys");
            book.setYear(2025);

            bookManager.addBook(book);

            List<Book> bookList = Arrays.asList(bookManager.searchByAuthor("Autorek"));
            for (var element: bookList
                 ) {
                System.out.println("Autor: " + element.getAuthor());
                System.out.println("Tytul: " + element.getTitle());
                System.out.println("Wydawca: " + element.getPublisher());
                System.out.println("\n");
            }
        } catch (RemoteException | ServiceException axisFault) {
            axisFault.printStackTrace();
        }
    }
}